import sys
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
import os

# Try to import models.py
# If it fails, we want to know about it, and we exit.
try:
    import models
except:
    print("Failed to import models.py. Exiting.")
    sys.exit(1)
    
    
# Initialize sqlalchemy + marshmallow
# These are used to interact with the database.
# If they fail to initialize, we want to know about it, and we exit.
try:
    DatabaseURL = os.environ.get('SHIX_API_DB_URL')
    engine = create_engine(DatabaseURL)
    database = Session(engine)
    md = models.metadata
    md.bind = engine
    print("Initilized database engine.")
except:
    print('Failed to initialize database engine. Exiting.')
    raise

# Create Tables
# If it fails, we want to know about it, and we exit.
# If it succeeds, we print a success message.
try:
    md.create_all()
except:
    raise Exception("Failed to create tables")
else:
    print("Created tables!")