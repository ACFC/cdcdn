import logging
import os, sys, io
import subprocess
import time
import jwt as pyjwt
import models
import sentry_sdk
import passlib
from passlib.hash import argon2
from fastapi import (Body, Depends, FastAPI, File, Form, Header, HTTPException, UploadFile, Response)
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from uuid import uuid4
from utils import *
import schemas
from minio import Minio
from minio.error import S3Error
#from elasticsearch import Elasticsearch, helpers
import hashlib
import datetime
import magic
import math

# Set up logging
try:
    # Desired log level is set by the LOG_LEVEL environment variable.
    # If it's not set, it defaults to INFO.
    # If it's set to an invalid value, it defaults to INFO.
    # If it's set to a valid value, it uses that value.
    # Valid values are: DEBUG, INFO, WARNING, ERROR, CRITICAL
    # This is done so that we can set the log level in the kubernetes deployment.
    desired_log_level = os.environ.get('LOG_LEVEL')
    VaildLogLevels = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
    if desired_log_level not in VaildLogLevels or not desired_log_level:
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
        logging.info("Logging initialized with invalid or no log level set. Defaulting to INFO.")
    else:
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.getLevelName(desired_log_level))
        logging.debug("Logging initialized. Desired log level: %s" % desired_log_level)
except:
    sys.exit(1) 
    # Logging should never fail, but if it does, we want to know about it.
    # If it does fail, there are bigger problems than just logging, since logging is a standard library.
    # But because we can't log it, we just exit.
    # sys.exit(1) is used to indicate a non-zero exit code, which is used by the systemd service to indicate a failure.
    # Hopefully this should kill the pod in kubernetes, but I'm not sure.


# Get start time of script
# This is used to calculate uptime, which is exposed in the status endpoint.
ServerStartTime = time.time()
logging.debug("Server start time: %s" % ServerStartTime)

# Initialize Sentry
# Sentry is used for error tracking.
# Should be initialized before anything else, so that we can track errors in initialization.
# But it's not critical, so if it fails, we just log it and continue.
try:
    sentry_dsn = os.environ.get('SHIX_API_SENTRY_DSN')
    sentry_sdk.init(
        dsn=f"{sentry_dsn}",

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production,
        traces_sample_rate=1.0,
    )
except:
    logging.error("Failed to initialize Sentry. Proceeding without error tracking.")
    pass


# Initialize FastAPI
# FastAPI is the web framework used to serve the API.
# If it fails to initialize, we want to know about it, and we exit.
try:
    app = FastAPI()
    logging.info("Initialized FastAPI.")
except:
    logging.critical("Failed to initialize FastAPI. Exiting.")
    sys.exit(1)

# Initialize sqlalchemy + marshmallow
# These are used to interact with the database.
# If they fail to initialize, we want to know about it, and we exit.
try:
    DatabaseURL = os.environ.get('SHIX_API_DB_URL')
    engine = create_engine(DatabaseURL)
    database = Session(engine)
    databaseModel = models.metadata
    databaseModel.bind = engine
    logging.info("Initilized database engine.")
except Exception as err:
    logging.critical('Failed to initialize database engine. Exiting.')
    sys.exit(1)
    
    
    
# Initialize Minio
# Minio is used to store files.
# If it fails to initialize, we want to know about it, and we exit.
try:
    MinioClient = Minio(
        os.environ.get('SHIX_API_MINIO_ADDRESS'),
        access_key=os.environ.get('SHIX_API_MINIO_ACCESS_KEY'),
        secret_key=os.environ.get('SHIX_API_MINIO_SECRET_KEY'),
        secure=False
    )
    logging.debug("Initialized Minio client.")
except:
    logging.critical('Failed to initialize Minio client. Exiting.')
    sys.exit(1)
    
#  # Initialize Elasticsearch
# try:
#     ElasticsearchClient = Elasticsearch([os.environ.get('SHIX_API_ELASTICSEARCH_ADDRESS')])
#     logging.debug("Initialized Elasticsearch client.")
# except:
#     logging.critical('Failed to initialize Elasticsearch client. Exiting.')
#     sys.exit(1)
    
    
# Validate JWT tokens
# This is used to validate JWT tokens.
async def ValidateJWT(authorization: str = Header()):
    """Authenticate a user by a JWT in the Authorization header"""

    if authorization.startswith("JWT"):
        jwt = authorization.replace("JWT ", "")
        try:
            jwt = pyjwt.decode(
                jwt,
                os.environ.get('SHIX_API_JWT_SECRET'),
                algorithms=["HS256"],
                # Require basic claims and disable certain checks because we'll be doing them on our own in UTC.
                options={"require": ["iss", "aud", "sub", "exp", "nbf", "iat"], "verify_exp": False, "verify_nbf": False, "verify_iat": False},
                audience=[os.environ.get('SHIX_API_JWT_AUDIENCE')],
            )
        except pyjwt.PyJWTError as err:
            raise HTTPException(status_code=401, detail="Token is invalid")

        if datetime.datetime.fromtimestamp(jwt['nbf']) > datetime.datetime.utcnow():
            raise HTTPException(status_code=401, detail="The token is not yet valid")
        elif datetime.datetime.fromtimestamp(jwt['exp']) < datetime.datetime.utcnow():
            raise HTTPException(status_code=401, detail="The token has expired")
        elif not isinstance(jwt['iat'], float) and not isinstance(jwt['iat'], int):
            raise HTTPException(status_code=401, detail="The token's issued time is invalid")

        account = database.query(models.User).where(models.User.id == jwt["sub"]).one_or_none()
        if not account:
            raise HTTPException(status_code=401, detail="The token's user does not exist.")
        elif not account.active:
            raise HTTPException(status_code=400, detail="Account is disabled")
        else:
            return schemas.User.from_orm(account)
    else:
        raise HTTPException(status_code=400, detail="Unknown authorization method")   

# Get a user from the database
async def GetUser(email: str = Body()):
    """Assert a User exists by their email and return the user"""
    user = database.query(models.User).filter_by(email=email).one_or_none()
    if user:
        return user
    else:
        raise HTTPException(status_code=400, detail="User does not exist")

# Verify a user is active
# This is used to verify a user is active.
async def IsActiveUser(user: schemas.User = Depends(GetUser)):
    """Assert a User exists and is active by their email"""
    if user.active:
        return schemas.User.from_orm(user)
    else:
        raise HTTPException(status_code=400, detail="Account is disabled")
    
    
    
# Define the root endpoint
# Should always return a static error message. since it's the root of the API, nothing is performed here.
# Ideally, this endpoint should never be called, but it's here just in case to not confuse users.
@app.get("/",
         status_code=200,
         name="API - Root",
         description="Return an error message",
         tags=['Information'])
async def root():
        logging.debug("Root endpoint called.")
        return {'success': False, 'detail': 'This is the root of the API. Please use a valid endpoint.'}



# Define the status endpoint
# Returns information about the API, including the git commit hash.
# Used to check if the API is running in health checks.
# If the git hash fails to be retrieved, it will notifiy the user that it could be running in a non-git environment.
@app.get("/status",
         status_code=201,
         name="API - Status",
         description="Return status information about the API",
         tags=['Information'])
async def root():
    logging.debug("Status endpoint called.")
    if not get_git_hash():
        CommitHash = "Could not get git hash. Could potentially be running in a non-git environment."
        logging.debug("Failed to get git hash, notifying user.")
    else:
        CommitHash = get_git_hash()[0:8]
        
    details = {
        'name': 'SHIX API - Forked for CDStore',
        'hostname': os.uname()[1],
        'commit': CommitHash
    }
    return {'success': True, 'detail': details}


# Define the user creation endpoint
# Creates a new user in the database.
# Requires a username, password, and email.
# If the username or email already exists, it will return an error.
# If the email is invalid, it will return an error.
# @app.post("/user/create",
#           status_code=200,
#           name="API - User Create",
#           description="Create a new user",
#           tags=['User'])
# async def user_create(user: schemas.APIUserCreate):
#     UniqueUserID = uuid4()
#     logging.debug("User create endpoint called")
#     if not verify_email(user.email):
#         logging.debug("Invalid email provided to the user create endpoint. email = %s" % user.email)
#         raise HTTPException(status_code=400, detail="Invalid email address")
#     if database.query(models.User).filter_by(username=user.username).first() or database.query(models.User).filter_by(email=user.email).first():
#         raise HTTPException(status_code=400, detail="Username or email is already taken")
#     try:
#         GenerateIdenticonResults = generate_identicon(user.username)
#         UploadIdenticonToMinio = MinioClient.put_object("cd-user-assets", f"avatar/{UniqueUserID}", io.BytesIO(GenerateIdenticonResults), len(GenerateIdenticonResults), metadata={"user": user.username, "type": "avatar", "filetype": "png", "id": f"{UniqueUserID}"})
#     except:
#         logging.error("Failed to upload identicon to Minio")
#         raise
#         #raise HTTPException(status_code=500, detail="Failed to upload identicon to Minio")
#     
#     now = GetSQLTime()
#     account = models.User(
#                     id = UniqueUserID,
#                     username = user.username,
#                     email = user.email,
#                     password = hash_password(user.password),
#                     avatar = UploadIdenticonToMinio.object_name,
#                     active = True,
#                     created = now,
#                     lastActivity = now
#                 )
#     database.add(account)
#     database.commit()
#     return{
#         "success": True,
#         "detail": "Account has been created",
#         "username": user.username,
#         "email": user.email,
#         "avatar": UploadIdenticonToMinio.object_name
#     }
# 
#     
#     
# Define the file retrieval endpoint
# Retrieves a file from minio
@app.get("/file/{location}/{file_hash}",
            status_code=200,
            name="API - File Retrieve",
            description="Retrieve a file from Minio",
            tags=['File'])
async def file_retrieve(file_hash: str, location: str):
    logging.debug("File retrieve endpoint called. file_hash = %s" % file_hash)
    # Check if the location is valid,
    VaildFileLocations = ["avatar", "artifact"]
    if location not in VaildFileLocations:
        raise HTTPException(status_code=400, detail="Invalid file location")
    if location == "avatar":
        FileObject = MinioClient.get_object("cd-user-assets", f"avatar/{file_hash}")
        return Response(content=FileObject.data, media_type=magic.from_buffer(FileObject.data, mime=True))
    if location == "artifact":
        fileInstance = database.query(models.File).filter_by(hash=file_hash).first()
        if not fileInstance:
            raise HTTPException(status_code=404, detail="File not found")
        adjustAccessCount = database.query(models.File).filter_by(hash=file_hash).one_or_none()
        adjustAccessCount.accesscount = adjustAccessCount.accesscount + 1
        database.add(adjustAccessCount)
        database.commit()
        
        if fileInstance.deleted:
            details = {
                "msg": "File has been deleted",
                "deleted": fileInstance.deleted,
                "reason": fileInstance.deletionreason
            }
            raise HTTPException(status_code=404, detail=details)
        FileObject = MinioClient.get_object("cd-content", f"{file_hash}")
        return Response(content=FileObject.data, media_type=magic.from_buffer(FileObject.data, mime=True))
    
    
## Define the user login endpoint
@app.post("/user/login",
          status_code=200,
          name="API - User Login",
          description="Login to an existing user",
          tags=['User'])
async def user_login(password: str = Body(), account: schemas.User = Depends(IsActiveUser)):
    if not verify_password(password, account.password.get_secret_value()):
        raise HTTPException(status_code=400, detail="Invaild credentials")
    AccountInDatabase = database.query(models.User).filter_by(email=account.email).one_or_none()
    AccountInDatabase.lastActivity = GetSQLTime()
    database.add(AccountInDatabase)
    database.commit()
    GenerateJWT = pyjwt.encode(
                {
                    "iss": os.environ.get('SHIX_API_JWT_ISSUER'),
                    "sub": account.id,
                    "aud": os.environ.get('SHIX_API_JWT_AUDIENCE'),
                    "exp": (datetime.datetime.utcnow() + datetime.timedelta(days=1)).timestamp(),
                    "nbf": datetime.datetime.utcnow().timestamp(),
                    "iat": datetime.datetime.utcnow().timestamp()
                },
                os.environ.get('SHIX_API_JWT_SECRET'),
                algorithm="HS256"
            )
    result = {
        "success": True,
        "detail": "Logged into account!",
        "avatar": os.environ.get('SHIX_API_ROOT_URL') + "/file/" + account.avatar,
        "username": account.username,
        "sessionkey": "JWT " + GenerateJWT
    }
    return result


# API Upload File
# This is the only endpoint which uses a multipart-form.
@app.post(
    '/file/upload', 
    status_code=201, 
    name="Artifact Upload", 
    description="Upload A file as an artifact to SHIX.\n\nThis is the only endpoint which uses a multipart-form.",
    tags=['Artifact']
    )
async def apiUploadFile(files: list[UploadFile], tag: str = Body(), account: schemas.User = Depends(ValidateJWT)):
    AllowedMimeType = ["image/webp", "video/webm", "image/png", "image/jpeg", "image/gif", "video/mp4", "video/mpeg", "audio/mpeg", "video/3gpp", "audio/3gpp", "video/3gpp2", "audio/3gpp2", "video/x-m4v", "video/x-matroska", "x-msvideo", "video/quicktime", "x-msvideo"]
    try:
        UploadedFiles = []
        userInDatabase = database.query(models.User).where(models.User.id == account.id).first()
        for file in files:
            DidUpload = False
            try:
                UniqueFileID = uuid4()
                fileContents = await file.read()
                FileHash = hashlib.sha256(fileContents).hexdigest()
                GussedFileType = magic.from_buffer(fileContents, mime=True)
                if str(GussedFileType) not in AllowedMimeType:
                    DidUpload = False
                    Detail = "File type not allowed"
                elif database.query(models.File).filter_by(hash=FileHash).first():
                    DidUpload = False
                    Detail = "File already exists"
                else:
                    now = GetSQLTime()
                    MinioClient.put_object("cd-content", FileHash, io.BytesIO(fileContents), len(fileContents), metadata={"UploadUser": account.id, "type": "artifact", "hash": FileHash, "filename": file.filename}, part_size=1024*1024*10,)
                    file = models.File(
                            id = UniqueFileID,
                            hash = FileHash,
                            filename = file.filename,
                            creationdate = now,
                            mimetype = GussedFileType,
                            tags = tag
                    )
                    userFileAssociation = models.UserFile(user=userInDatabase, file=file)
                    database.add(userFileAssociation)
                    database.add(file)
                    #userInDatabase.files.append(file)
                    #Index file to elasticsearch
                    #ElasticsearchClient.index(index="shix_files", body={"id": UniqueFileID, "hash": FileHash, "filename": file.filename, "creationdate": time.time(), "uploaduserid": account.id, "mimetype": GussedFileType})
                    DidUpload = True
                    Detail = "File uploaded"
            except:
                raise
                logging.error("Failed to upload file to backend %s" % FileHash)
                Detail = "Failed to upload file to backend"
                DidUpload = False
            fileInfo = {
                "success": DidUpload,
                "detail": Detail,
                "filename": file.filename,
                "mimetype": GussedFileType,
                "hash": FileHash,
                "uploaded": GetSQLTime(),
                "epoch": time.time(),
                "tag": tag,
            }
            UploadedFiles.append(fileInfo)
        database.commit()
        return {"success": True, "count": len(UploadedFiles), "detail": UploadedFiles}
    except:
        raise #HTTPException(status_code=500, detail="Failed to upload file(s) to backend")
    
 
 # API Get files with tag
@app.post('/file/tag', status_code=200, name="Artifact Tag", description="Get files with a specific tag.", tags=['Artifact'])
async def getFilesWithTag(instance: schemas.APIGetByTag, account: schemas.User = Depends(ValidateJWT)):
    try:
        if not database.query(models.File).filter_by(tags=instance.tag).all():
            return HTTPException(status_code=404, detail="No files with that tag", headers={"X-Error": "No files with that tag"})
        else:
            Returnedfiles = []
            files = database.query(models.File).filter_by(tags=instance.tag).all()
            for i in files:
                Returnedfiles.append(os.environ.get("SHIX_API_ROOT_URL") + "/file/artifact/" + i.hash)
            return {"success": True, "count": len(files), "detail": Returnedfiles}
    except:
        raise #HTTPException(status_code=500, detail="Failed to get files with tag")
    
    
# API Search File
# Search for files by multiple parameters
# This endpoint can return specific search result
# Or even pages of search results
#@app.post('/file/search',
#          status_code=200,
#          name="Artifact Search",
#          description="Search for files by multiple parameters.\n\nThis endpoint can return specific search result or even pages of search results.",
#          tags=['Artifact'])
#async def searchFile(query: schemas.APISearch, account: schemas.User = Depends(ValidateJWT)):
#    try:
#        if query.page == 0:
#            return {"success": False, "detail": "Page number cannot be 0"}
#        RequestTimeStart = time.time()
#        PageSize = 50 #Set page size, static for now
#        SearchResults = ElasticsearchClient.search(index="shix_files", body={
#        "query": {
#            "match_all": {}
#        },
#        "sort": [{"creationdate": {"order": "desc"}}],
#        "from_": (query.page - 1) * PageSize,
#        "size": PageSize
#        }) 
#
#        PageCount = math.ceil(SearchResults["hits"]["total"]["value"] / PageSize) 
#        RequestTimeInSeconds = time.time() - RequestTimeStart
#        if SearchResults["hits"]["hits"] == []:
#            return {"success": True, "count": 0, "time": RequestTimeInSeconds, "detail": "No results found"}
#        else:
#            return {"success": True, "count": len(SearchResults["hits"]["hits"]), "time": RequestTimeInSeconds, "pages": PageCount, "detail": "Results found", "results": SearchResults["hits"]["hits"]}
#    except:
#        raise HTTPException(status_code=500, detail="Failed to search for files")
#
#
# API Get User
# Get a user by their ID or username or jwt tokens