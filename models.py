import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy as FlaskSQLAlchemy
from flask_marshmallow import Marshmallow


metadata = db.MetaData()
Base = declarative_base(metadata=metadata)

class User(Base):
    __tablename__ = 'users'
    id = db.Column(db.String, primary_key=True, unique=True, nullable=False)
    username = db.Column(db.String, unique=True, nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.String, unique=False, nullable=False)
    #totp = db.Column(db.Boolean, unique=False, nullable=False, server_default="0")
    #totpSecret = db.Column(db.String, unique=False, nullable=True)
    avatar = db.Column(db.String, unique=False, nullable=False)
    active = db.Column(db.Boolean, unique=False, nullable=False, server_default="1")
    created = db.Column(db.DateTime, unique=False, nullable=False)
    settings = db.Column(db.JSON, unique=False, nullable=True)
    lastActivity = db.Column(db.DateTime, unique=False, nullable=True)
    files = relationship("UserFile", back_populates="user")

class File(Base):
    __tablename__ = 'files'
    id = db.Column(db.String, primary_key=True, unique=True, nullable=False)
    hash = db.Column(db.String, unique=True, nullable=False)
    filename = db.Column(db.String, unique=True, nullable=False)
    mimetype = db.Column(db.String, unique=False, nullable=False)
    creationdate = db.Column(db.DateTime, unique=False, nullable=False)
    users = relationship("UserFile", back_populates="file")
    accesscount = db.Column(db.Integer, unique=False, nullable=True, server_default="0")
    tags = db.Column(db.String, unique=False, nullable=True)
    deleted = db.Column(db.Boolean, unique=False, nullable=False, server_default="0")
    deletionreason = db.Column(db.String, unique=False, nullable=True)
    
class UserFile(Base):
    __tablename__ = 'userfiles'
    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False)
    userid = db.Column(db.String, db.ForeignKey('users.id'), nullable=False)
    fileid = db.Column(db.String, db.ForeignKey('files.id'), nullable=False)
    user = relationship("User", back_populates="files")
    file = relationship("File", back_populates="users")