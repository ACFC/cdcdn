from __future__ import annotations
from typing import Optional, List, Dict
import uuid, json
from uuid import UUID

from pydantic import BaseModel, Field, Json, SecretStr
from datetime import datetime

class User(BaseModel):
    class Config:
        orm_mode = True

    id: str
    username: str
    email: str
    password: SecretStr
    # totp: bool = False
    # totpSecret: SecretStr = None
    avatar: str
    active: bool
    created: datetime
    settings: Optional[Json]
    favorites: Optional[Json]
    lastActivity: datetime

# API Models


class APIUserCreate(BaseModel):
    username: str
    email: str
    password: str

class APIGetByTag(BaseModel):
    tag: str


class APISearch(BaseModel):
    query: Optional[str]
    page: int = 1
    
class APIUserSearch(BaseModel):
    userid: Optional[str]
    username: Optional[str]
    