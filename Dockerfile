FROM python:3.7

RUN mkdir /app
WORKDIR /app
ADD . /app/
RUN pip install -r requirements.txt

EXPOSE 5000
CMD python -m uvicorn main:app --host 0.0.0.0 --port 5000
